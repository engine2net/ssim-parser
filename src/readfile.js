const fs = require('fs')

const readFile = (file, encoding = 'utf8') => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, encoding, (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

module.exports = readFile
