import readFile from '../src/readfile'

it('reads file correctly', () => {
  return readFile(__dirname + '/test.ssim').then(result => {
    expect(result.startsWith('1AIRLINE')).toBe(true)
  })
})

test('throws when error reading', () => {
  return readFile(__dirname + '/nonexisting.file').catch(err => {
    expect(err).toBeDefined()
  })
})
