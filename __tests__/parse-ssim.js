import readFile from '../src/readfile'
import parseSSIM from '../index'

it('parses SSIM file', () => {
  const expected = [
    {
      airlineDesignator: 'QS',
      flightNumber: '1012',
      periodOfOperationFrom: '2016-05-28',
      periodOfOperationTo: '2016-10-01',
      daysOfOperation: [ 0 ],
      depIATA: 'PRG',
      passengerSTD: '1135',
      passengerTerminalOfDepartureStation: '2',
      arrIATA: 'NAP',
      passengerSTA: '1330',
      passengerTerminalOfArrivalStation: '',
      aircraftType: '738',
      dates: [{
        departure: '2016-05-29T11:35:00Z',
        arrival: '2016-05-29T13:30:00Z'
      }, {
        departure: '2016-06-05T11:35:00Z',
        arrival: '2016-06-05T13:30:00Z'
      }, {
        departure: '2016-06-12T11:35:00Z',
        arrival: '2016-06-12T13:30:00Z'
      }, {
        departure: '2016-06-19T11:35:00Z',
        arrival: '2016-06-19T13:30:00Z'
      }, {
        departure: '2016-06-26T11:35:00Z',
        arrival: '2016-06-26T13:30:00Z'
      }, {
        departure: '2016-07-03T11:35:00Z',
        arrival: '2016-07-03T13:30:00Z'
      }, {
        departure: '2016-07-10T11:35:00Z',
        arrival: '2016-07-10T13:30:00Z'
      }, {
        departure: '2016-07-17T11:35:00Z',
        arrival: '2016-07-17T13:30:00Z'
      }, {
        departure: '2016-07-24T11:35:00Z',
        arrival: '2016-07-24T13:30:00Z'
      }, {
        departure: '2016-07-31T11:35:00Z',
        arrival: '2016-07-31T13:30:00Z'
      }, {
        departure: '2016-08-07T11:35:00Z',
        arrival: '2016-08-07T13:30:00Z'
      }, {
        departure: '2016-08-14T11:35:00Z',
        arrival: '2016-08-14T13:30:00Z'
      }, {
        departure: '2016-08-21T11:35:00Z',
        arrival: '2016-08-21T13:30:00Z'
      }, {
        departure: '2016-08-28T11:35:00Z',
        arrival: '2016-08-28T13:30:00Z'
      }, {
        departure: '2016-09-04T11:35:00Z',
        arrival: '2016-09-04T13:30:00Z'
      }, {
        departure: '2016-09-11T11:35:00Z',
        arrival: '2016-09-11T13:30:00Z'
      }, {
        departure: '2016-09-18T11:35:00Z',
        arrival: '2016-09-18T13:30:00Z'
      }, {
        departure: '2016-09-25T11:35:00Z',
        arrival: '2016-09-25T13:30:00Z'
      }]
    }, {
      airlineDesignator: 'QS',
      flightNumber: '1113',
      periodOfOperationFrom: '2016-05-24',
      periodOfOperationTo: '2016-05-24',
      daysOfOperation: [ 2 ],
      depIATA: 'RHO',
      passengerSTD: '2255',
      passengerTerminalOfDepartureStation: '',
      arrIATA: 'PRG',
      passengerSTA: '0055',
      passengerTerminalOfArrivalStation: '2',
      aircraftType: '738',
      dates: [{
        departure: '2016-05-24T22:55:00Z',
        arrival: '2016-05-25T00:55:00Z'
      }]
    }
  ]

  return readFile(__dirname + '/test.ssim').then(parseSSIM).then(result => {
    expect(result).toEqual(expected)
  })
})

test('throws on bad input', () => {
  expect(() => parseSSIM('error input', { validate: true })).toThrowError('Not a SSIM file.')
})
