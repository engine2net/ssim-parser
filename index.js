const moment = require('moment')
const isSSIM = require('is-ssim')

const parseSSIM = (ssim, options) => {
  if (options && options.validate) {
    try {
      isSSIM(ssim)
    } catch (err) {
      throw (err)
    }
  }

  return ssim
    .trim()
    .split('\n')
    .filter(row => row.charAt(0) === '3' && row.charAt(13) === 'J')
    .map(row => {
      let fromDate = moment(row.slice(14, 21), 'DDMMMYY')
      let toDate = moment(row.slice(21, 28), 'DDMMMYY')
      const nextYear = moment().add(1, 'years')
      const format = 'YYYY-MM-DD'

      // The XXX scenario.
      if (!toDate.isValid()) {
        if (fromDate.isAfter(nextYear)) {
          toDate = fromDate.clone().add(1, 'months')
        } else {
          toDate = nextYear
        }
      }

      return {
        airlineDesignator: row.slice(2, 5).trim(),
        flightNumber: row.slice(5, 9),
        periodOfOperationFrom: fromDate.format(format),
        periodOfOperationTo: toDate.format(format),
        daysOfOperation: row.slice(28, 35).split('').filter(isDay).map(fixSunday),
        depIATA: row.slice(36, 39),
        passengerSTD: row.slice(39, 43),
        passengerTerminalOfDepartureStation: row.slice(52, 54).trim(),
        arrIATA: row.slice(54, 57),
        passengerSTA: row.slice(61, 65),
        passengerTerminalOfArrivalStation: row.slice(70, 72).trim(),
        aircraftType: row.slice(72, 75)
      }
    })
    .map(flight => {
      const {
        passengerSTD: std,
        passengerSTA: sta,
        daysOfOperation: weekDays,
        periodOfOperationFrom: operatingFrom,
        periodOfOperationTo: operatingTo
      } = flight
      const overMidnight = fliesOverMidnight(std, sta)
      const stdISO = getISOTime(std)
      const from = moment.utc(`${operatingFrom}T${stdISO}`)
      const to = moment.utc(`${operatingTo}T${stdISO}`)

      let dates = []
      let depDateTime = from.clone()
      let arrDateTime = from.clone().set(getTimeObject(sta))
      if (overMidnight) {
        arrDateTime.add(1, 'days')
      }

      while (depDateTime.diff(to) <= 0) {
        let day = depDateTime.day()

        if (weekDays.includes(day)) {
          dates.push({
            departure: depDateTime.format(),
            arrival: arrDateTime.format()
          })
        }

        depDateTime.add(1, 'days')
        arrDateTime.add(1, 'days')
      }

      return Object.assign({}, flight, {
        dates
      })
    })
}

const isDay = day => day !== ' '

const fixSunday = day => {
  if (day === '7') {
    day = 0
  }

  return parseInt(day, 10)
}

const getTimeObject = time => {
  const hour = parseInt(time.slice(0, 2), 10)
  const minute = parseInt(time.slice(2), 10)

  return { hour, minute }
}

const getISOTime = time => {
  const hh = time.slice(0, 2)
  const mm = time.slice(2)

  return `${hh}:${mm}:00`
}

const fliesOverMidnight = (std, sta) => {
  return parseInt(sta, 10) - parseInt(std, 10) < 0
}

module.exports = parseSSIM
